package com.example.happytravelapp.viewmodel;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.happytravelapp.data.FirebaseQueryLiveData;
import com.example.happytravelapp.model.Hotel;
import com.example.happytravelapp.ultil.Common;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HotelViewModel extends ViewModel {
    private static final DatabaseReference HOTEL_REF = FirebaseDatabase.getInstance().getReference("/hotels");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOTEL_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private List<Hotel> mList = new ArrayList<>();
    private final LiveData<List<Hotel>> hotelListLivedata = Transformations.map(liveData, new DeserializerList());
    public LiveData<List<Hotel>> getHotelListLivedata() {
        return hotelListLivedata;
    }
    private class DeserializerList implements Function<DataSnapshot, List<Hotel>> {
        @Override
        public List<Hotel> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                Hotel hotel = snap.getValue(Hotel.class);
                mList.add(hotel);
            }
            return mList;
        }
    }


    public MutableLiveData<String> imageUrl = new MutableLiveData<>();
    public MutableLiveData<String> hotelName = new MutableLiveData<>();
    public MutableLiveData<String> provine = new MutableLiveData<>();
    public MutableLiveData<String> district = new MutableLiveData<>();
    public MutableLiveData<String> standardStar = new MutableLiveData<>();

    public void getHotelDetail(String uid) {
        HOTEL_REF.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Hotel hotel = new Hotel();
                hotel = snapshot.getValue(Hotel.class);
                imageUrl.setValue(hotel.getImageUrl());
                hotelName.setValue(hotel.getHotelName());
                provine.setValue(hotel.getProvine());
                district.setValue(hotel.getDistrict());
                standardStar.setValue(hotel.getStandardStar());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }



    private List<Hotel> mList2 = new ArrayList<>();
    private final LiveData<List<Hotel>> hotelListLivedata2 = Transformations.map(liveData, new DeserializerList2());
    public LiveData<List<Hotel>> getHotelListLivedata2() {
        return hotelListLivedata2;
    }
    private class DeserializerList2 implements Function<DataSnapshot, List<Hotel>> {
        @Override
        public List<Hotel> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                if (snap.child("provine").getValue().toString().equalsIgnoreCase("Thành phố Hồ Chí Minh")) {
                    Hotel hotel = snap.getValue(Hotel.class);
                    mList2.add(hotel);
                }
            }
            return mList2;
        }
    }

    private List<Hotel> mList3 = new ArrayList<>();
    private final LiveData<List<Hotel>> hotelListLivedata3 = Transformations.map(liveData, new DeserializerList3());
    public LiveData<List<Hotel>> getHotelListLivedata3() {
        return hotelListLivedata3;
    }
    private class DeserializerList3 implements Function<DataSnapshot, List<Hotel>> {
        @Override
        public List<Hotel> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                if (snap.child("provine").getValue().toString().equalsIgnoreCase("Thành phố Hà Nội")) {
                    Hotel hotel = snap.getValue(Hotel.class);
                    mList3.add(hotel);
                }
            }
            return mList3;
        }
    }

    public static String getHotelName(String hotelId){
        HOTEL_REF.child(hotelId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Common.HOTEL_NAME=snapshot.child("hotelName").getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return Common.HOTEL_NAME;
    }
}
