package com.example.happytravelapp.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.happytravelapp.R;
import com.example.happytravelapp.databinding.FragmentAccountBinding;
import com.example.happytravelapp.view.BookingManagerActivity;
import com.example.happytravelapp.view.LoginActivity;
import com.example.happytravelapp.view.MainActivity;
import com.example.happytravelapp.view.ProfileActivity;
import com.example.happytravelapp.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    FragmentAccountBinding binding;
    UserViewmodel userViewmodel;


    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAccountBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userViewmodel= ViewModelProviders.of(this).get(UserViewmodel.class);


        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    ((MainActivity)getContext()).getSupportActionBar().setTitle("Xin chào "+firebaseUser.getDisplayName());
                } else {

                }
            }
        });

        binding.profile.setOnClickListener(v->{
            startActivity(new Intent(getContext(), ProfileActivity.class));
        });

        binding.bookingManager.setOnClickListener(v->{
            startActivity(new Intent(getContext(), BookingManagerActivity.class));
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.account_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            userViewmodel.logout();
            startActivity(new Intent(getContext(), LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
